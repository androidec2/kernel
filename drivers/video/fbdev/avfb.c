/*
 *  linux/drivers/video/avfb.c - Virtual frame buffer for Android
 *  Based on linux/drivers/video/vfb.c
 *
 *  Copyright (C) 2016 Marcin Chojnacki
 *  Copyright (C) 2002 James Simmons
 *  Copyright (C) 1997 Geert Uytterhoeven
 *
 *  This file is subject to the terms and conditions of the GNU General Public
 *  License. See the file COPYING in the main directory of this archive for
 *  more details.
 */

#include <linux/fb.h>
#include <linux/mm.h>
#include <linux/platform_device.h>

/*
 * User-friendly defines
 */
#define ANDROID_WIDTH   720
#define ANDROID_HEIGHT  1200
#define ANDROID_BUFFERS 2
#define ANDROID_BPP     4

/*
 * RAM we reserve for the frame buffer. This defines the maximum screen
 * size
 */
static void *vram = NULL;
static unsigned long vram_size    = 0;
static unsigned long vram_aligned = 0;

/*
 * Memory management
 */
static void *rvmalloc(unsigned long size)
{
    void *mem;
    unsigned long adr;

    mem = vmalloc_32(size);
    if (!mem)
        return NULL;

    /*
     * AVFB must clear memory to prevent kernel info
     * leakage into userspace
     * VGA-based drivers MUST NOT clear memory if
     * they want to be able to take over vgacon
     */
    memset(mem, 0, size); /* Clear the ram out, no junk to the user */
    adr = (unsigned long)mem;
    while (size > 0) {
        SetPageReserved(vmalloc_to_page((void *)adr));
        adr += PAGE_SIZE;
        size -= PAGE_SIZE;
    }

    return mem;
}

static void rvfree(void *mem, unsigned long size)
{
    unsigned long adr;

    if (!mem)
        return;

    adr = (unsigned long)mem;
    while ((long)size > 0) {
        ClearPageReserved(vmalloc_to_page((void *)adr));
        adr += PAGE_SIZE;
        size -= PAGE_SIZE;
    }
    vfree(mem);
}

static struct fb_var_screeninfo avfb_default = {
    .xres           = ANDROID_WIDTH,
    .yres           = ANDROID_HEIGHT,
    .xres_virtual   = ANDROID_WIDTH,
    .yres_virtual   = ANDROID_HEIGHT * ANDROID_BUFFERS,
    .bits_per_pixel = ANDROID_BPP * 8,
    .activate       = FB_ACTIVATE_NOW,
    .height         = 0,
    .width          = 0,
    .pixclock       = 0,
#if ANDROID_BPP == 2
    .red    = {11, 5, 0},
    .green  = {5, 6, 0},
    .blue   = {0, 5, 0},
    .transp = {0, 0, 0},
#elif ANDROID_BPP == 4
    .red    = {16, 8, 0},
    .green  = {8, 8, 0},
    .blue   = {0, 8, 0},
    .transp = {0, 0, 0},
#endif
};

static struct fb_fix_screeninfo avfb_fix = {
    .id          = "Android VFB",
    .type        = FB_TYPE_PACKED_PIXELS,
    .visual      = FB_VISUAL_TRUECOLOR,
    .line_length = ANDROID_WIDTH * ANDROID_BPP,
    .accel       = FB_ACCEL_NONE,
    .ypanstep    = 1,
};

static u32 cmap[16];

static int avfb_check_var(struct fb_var_screeninfo *var, struct fb_info *info);
static int avfb_set_par(struct fb_info *info);
static int avfb_setcolreg(unsigned regno, unsigned red, unsigned green,
                          unsigned blue, unsigned transp, struct fb_info *info);
static int avfb_pan_display(struct fb_var_screeninfo *var,
                            struct fb_info *info);
static int avfb_mmap(struct fb_info *info, struct vm_area_struct *vma);

static struct fb_ops avfb_ops = {
    .fb_check_var   = avfb_check_var,
    .fb_set_par     = avfb_set_par,
    .fb_setcolreg   = avfb_setcolreg,
    .fb_pan_display = avfb_pan_display,
    .fb_fillrect    = sys_fillrect,
    .fb_copyarea    = sys_copyarea,
    .fb_imageblit   = sys_imageblit,
    .fb_mmap        = avfb_mmap,
};

/*
 * Setting the video mode has been split into two parts.
 * First part, xxxfb_check_var, must not write anything
 * to hardware, it should only verify and adjust var.
 * This means it doesn't alter par but it does use hardware
 * data from it to check this var.
 */
static int avfb_check_var(struct fb_var_screeninfo *var, struct fb_info *info)
{
    /*
     * Some basic checks from goldfish
     */
    if ((var->xres != info->var.xres) ||
        (var->yres != info->var.yres) ||
        (var->xres_virtual != info->var.xres) ||
        (var->yres_virtual > info->var.yres * 2) ||
        (var->yres_virtual < info->var.yres)) {
        return -EINVAL;
    }

    if ((var->xoffset != info->var.xoffset) ||
        (var->bits_per_pixel != info->var.bits_per_pixel) ||
        (var->grayscale != info->var.grayscale)) {
        return -EINVAL;
    }

    /*
     * Memory limit (temporary)
     */
    if (var->xres_virtual * var->yres_virtual * ANDROID_BPP > vram_size) {
        return -ENOMEM;
    }

    return 0;
}

/* This routine actually sets the video mode. It's in here where we
 * the hardware state info->par and fix which can be affected by the
 * change in par. For this driver it doesn't do much.
 */
static int avfb_set_par(struct fb_info *info)
{
    /* NOOP */
    return 0;
}

static inline u32 convert_bitfield(int val, struct fb_bitfield *bf)
{
    unsigned mask = (1 << bf->length) - 1;
    return (val >> (16 - bf->length) & mask) << bf->offset;
}

/*
 * Set a single color register. The values supplied are already
 * rounded down to the hardware's capabilities (according to the
 * entries in the var structure). Return != 0 for invalid regno.
 */
static int avfb_setcolreg(unsigned regno, unsigned red, unsigned green,
                          unsigned blue, unsigned transp, struct fb_info *info)
{
    if (regno < 16) {
        cmap[regno] = convert_bitfield(transp, &info->var.transp) |
                      convert_bitfield(blue, &info->var.blue) |
                      convert_bitfield(green, &info->var.green) |
                      convert_bitfield(red, &info->var.red);
        return 0;
    } else {
        return 1;
    }
}

/*
 * Pan or Wrap the Display
 *
 * This call looks only at xoffset, yoffset and the FB_VMODE_YWRAP flag
 */
static int avfb_pan_display(struct fb_var_screeninfo *var, struct fb_info *info)
{
    /* NOOP */
    return 0;
}

/*
 * Most drivers don't need their own mmap function
 */
static int avfb_mmap(struct fb_info *info, struct vm_area_struct *vma)
{
    unsigned long start  = vma->vm_start;
    unsigned long size   = vma->vm_end - vma->vm_start;
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
    unsigned long page, pos;

    if (vma->vm_pgoff > (~0UL >> PAGE_SHIFT))
        return -EINVAL;
    if (size > vram_aligned)
        return -EINVAL;
    if (offset > vram_aligned - size)
        return -EINVAL;

    pos = (unsigned long)info->fix.smem_start + offset;

    while (size > 0) {
        page = vmalloc_to_pfn((void *)pos);
        if (remap_pfn_range(vma, start, page, PAGE_SIZE, PAGE_SHARED)) {
            return -EAGAIN;
        }
        start += PAGE_SIZE;
        pos += PAGE_SIZE;
        if (size > PAGE_SIZE)
            size -= PAGE_SIZE;
        else
            size = 0;
    }

    return 0;
}

/*
 * Initialisation
 */
static int avfb_probe(struct platform_device *dev)
{
    struct fb_info *info;
    int retval = -ENOMEM;

    vram_size    = avfb_fix.line_length * avfb_default.yres_virtual;
    vram_aligned = PAGE_ALIGN(vram_size);

    /*
     * For real video cards we use ioremap.
     */
    if (!(vram = rvmalloc(vram_aligned)))
        return retval;

    info = framebuffer_alloc(sizeof(u32) * 256, &dev->dev);
    if (!info)
        goto err0;

    info->fbops          = &avfb_ops;
    info->flags          = FBINFO_FLAG_DEFAULT;
    info->pseudo_palette = cmap;
    info->screen_base    = (char __iomem *)vram;

    avfb_fix.smem_start = (unsigned long)vram;
    avfb_fix.smem_len   = vram_size;
    info->fix           = avfb_fix;

    info->var = avfb_default;

    retval = register_framebuffer(info);
    if (retval < 0)
        goto err1;

    platform_set_drvdata(dev, info);

    fb_info(info, "AVFB using %ldK of video memory\n", vram_aligned >> 10);
    return 0;

err1:
    framebuffer_release(info);
err0:
    rvfree(vram, vram_aligned);
    return retval;
}

static int avfb_remove(struct platform_device *dev)
{
    struct fb_info *info = platform_get_drvdata(dev);
    if (info) {
        unregister_framebuffer(info);
        rvfree(vram, vram_aligned);
        framebuffer_release(info);
    }

    return 0;
}

static struct platform_driver avfb_driver = {
    .probe  = avfb_probe,
    .remove = avfb_remove,
    .driver =
        {
            .name = "avfb",
        },
};

static struct platform_device *avfb_device;

static int __init avfb_init(void)
{
    int ret = platform_driver_register(&avfb_driver);
    if (!ret) {
        avfb_device = platform_device_alloc("avfb", 0);

        if (avfb_device)
            ret = platform_device_add(avfb_device);
        else
            ret = -ENOMEM;

        if (ret) {
            platform_device_put(avfb_device);
            platform_driver_unregister(&avfb_driver);
        }
    }

    return ret;
}

module_init(avfb_init);

#ifdef MODULE
static void __exit avfb_exit(void)
{
    platform_device_unregister(avfb_device);
    platform_driver_unregister(&avfb_driver);
}

module_exit(avfb_exit);

MODULE_LICENSE("GPL");
#endif